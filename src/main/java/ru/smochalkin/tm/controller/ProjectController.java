package ru.smochalkin.tm.controller;

import ru.smochalkin.tm.api.controller.IProjectController;
import ru.smochalkin.tm.api.service.IProjectService;
import ru.smochalkin.tm.enumerated.Sort;
import ru.smochalkin.tm.enumerated.Status;
import ru.smochalkin.tm.exception.system.SortNotFoundException;
import ru.smochalkin.tm.exception.system.StatusNotFoundException;
import ru.smochalkin.tm.model.Project;
import ru.smochalkin.tm.util.TerminalUtil;

import java.util.Arrays;
import java.util.List;

public class ProjectController implements IProjectController {

    private final IProjectService projectService;

    public ProjectController(IProjectService projectService) {
        this.projectService = projectService;
    }

    @Override
    public void showProjects() {
        System.out.println("[PROJECT LIST]");
        System.out.println("Enter sort option from list:");
        System.out.println(Arrays.toString(Sort.values()));
        List<Project> projects;
        String sortName = TerminalUtil.nextLine();
        if (sortName == null || sortName.isEmpty()) {
            projects = projectService.findAll();
        } else {
            Sort sort;
            try {
                sort = Sort.valueOf(sortName);
            } catch (IllegalArgumentException e) {
                throw new SortNotFoundException();
            }
            System.out.println(sort.getDisplayName());
            projects = projectService.findAll(sort.getComparator());
        }
        int index = 1;
        for (Project project : projects) {
            System.out.println(index++ + ". " + project);
        }
        System.out.println("[OK]");
    }

    @Override
    public void clearProjects() {
        System.out.println("[CLEAR PROJECTS]");
        projectService.clear();
        System.out.println("[OK]");
    }

    @Override
    public void createProject() {
        System.out.println("[CREATE PROJECT]");
        System.out.print("Enter name: ");
        String name = TerminalUtil.nextLine();
        System.out.print("Enter description: ");
        String description = TerminalUtil.nextLine();
        projectService.create(name, description);
        System.out.println("[OK]");
    }

    @Override
    public void showById() {
        System.out.print("Enter id: ");
        String id = TerminalUtil.nextLine();
        Project project = projectService.findById(id);
        showProject(project);
    }

    @Override
    public void showByName() {
        System.out.print("Enter name: ");
        String name = TerminalUtil.nextLine();
        Project project = projectService.findByName(name);
        showProject(project);
    }

    @Override
    public void showByIndex() {
        System.out.print("Enter index: ");
        Integer index = TerminalUtil.nextInt();
        Project project = projectService.findByIndex(--index);
        showProject(project);
    }

    @Override
    public void showProject(Project project) {
        System.out.println("Id: " + project.getId());
        System.out.println("Name: " + project.getName());
        System.out.println("Description: " + project.getDescription());
        System.out.println("Status: " + project.getStatus().getDisplayName());
        System.out.println("Created: " + project.getCreated());
        System.out.println("Start: " + project.getStartDate());
        System.out.println("End: " + project.getEndDate());
    }

    @Override
    public void updateById() {
        System.out.print("Enter id: ");
        String id = TerminalUtil.nextLine();
        projectService.findById(id);
        System.out.print("Enter new name: ");
        String name = TerminalUtil.nextLine();
        System.out.print("Enter new description: ");
        String desc = TerminalUtil.nextLine();
        projectService.updateById(id, name, desc);
    }

    @Override
    public void updateByIndex() {
        System.out.print("Enter index: ");
        Integer index = TerminalUtil.nextInt();
        index--;
        projectService.findByIndex(index);
        System.out.print("Enter new name: ");
        String name = TerminalUtil.nextLine();
        System.out.print("Enter new description: ");
        String desc = TerminalUtil.nextLine();
        projectService.updateByIndex(index, name, desc);
    }

    @Override
    public void startById() {
        System.out.print("Enter id: ");
        String id = TerminalUtil.nextLine();
        projectService.findById(id);
        projectService.updateStatusById(id, Status.IN_PROGRESS);
    }

    @Override
    public void startByName() {
        System.out.print("Enter name: ");
        String name = TerminalUtil.nextLine();
        projectService.findByName(name);
        projectService.updateStatusByName(name, Status.IN_PROGRESS);
    }

    @Override
    public void startByIndex() {
        System.out.print("Enter index: ");
        Integer index = TerminalUtil.nextInt();
        index--;
        projectService.findByIndex(index);
        projectService.updateStatusByIndex(index, Status.IN_PROGRESS);
    }

    @Override
    public void completeById() {
        System.out.print("Enter id: ");
        String id = TerminalUtil.nextLine();
        projectService.findById(id);
        projectService.updateStatusById(id, Status.COMPLETED);
    }

    @Override
    public void completeByName() {
        System.out.print("Enter name: ");
        String name = TerminalUtil.nextLine();
        projectService.findByName(name);
        projectService.updateStatusByName(name, Status.COMPLETED);
    }

    @Override
    public void completeByIndex() {
        System.out.print("Enter index: ");
        Integer index = TerminalUtil.nextInt();
        index--;
        projectService.findByIndex(index);
        projectService.updateStatusByIndex(index, Status.COMPLETED);
    }

    @Override
    public void updateStatusById() {
        System.out.print("Enter id: ");
        String id = TerminalUtil.nextLine();
        projectService.findById(id);
        System.out.println("Enter new status from list:");
        System.out.println(Arrays.toString(Status.values()));
        String statusName = TerminalUtil.nextLine();
        Status status;
        try {
            status = Status.valueOf(statusName);
        } catch (IllegalArgumentException e) {
            throw new StatusNotFoundException();
        }
        projectService.updateStatusById(id, status);
    }

    @Override
    public void updateStatusByName() {
        System.out.print("Enter name: ");
        String name = TerminalUtil.nextLine();
        projectService.findByName(name);
        System.out.println("Enter new status from list:");
        System.out.println(Arrays.toString(Status.values()));
        String statusName = TerminalUtil.nextLine();
        Status status;
        try {
            status = Status.valueOf(statusName);
        } catch (IllegalArgumentException e) {
            throw new StatusNotFoundException();
        }
        projectService.updateStatusByName(name, status);
    }

    @Override
    public void updateStatusByIndex() {
        System.out.print("Enter index: ");
        Integer index = TerminalUtil.nextInt();
        index--;
        projectService.findByIndex(index);
        System.out.println("Enter new status from list:");
        System.out.println(Arrays.toString(Status.values()));
        String statusName = TerminalUtil.nextLine();
        Status status;
        try {
            status = Status.valueOf(statusName);
        } catch (IllegalArgumentException e) {
            throw new StatusNotFoundException();
        }
        projectService.updateStatusByIndex(index, status);
    }

}
