package ru.smochalkin.tm.enumerated;

import ru.smochalkin.tm.comparator.*;
import java.util.Comparator;

public enum Sort {

    NAME("Sort by name", ComparatorByName.INSTANCE),
    STATUS("Sort by status", ComparatorByStatus.INSTANCE),
    CREATED("Sort by created", ComparatorByCreated.INSTANCE),
    START_DATE("Sort by start date", ComparatorByStartDate.INSTANCE),
    END_DATE("Sort by end date", ComparatorByEndDate.INSTANCE);

    private final String displayName;

    private final Comparator comparator;

    public String getDisplayName() {
        return displayName;
    }

    public Comparator getComparator() {
        return comparator;
    }

    Sort(String displayName, Comparator comparator) {
        this.displayName = displayName;
        this.comparator = comparator;
    }

}
