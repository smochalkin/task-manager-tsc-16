package ru.smochalkin.tm.api.model;

import ru.smochalkin.tm.enumerated.Status;

public interface IHasStatus {

    Status getStatus();

    void setStatus(Status status);

}
