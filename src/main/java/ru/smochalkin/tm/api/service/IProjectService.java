package ru.smochalkin.tm.api.service;

import ru.smochalkin.tm.enumerated.Status;
import ru.smochalkin.tm.model.Project;

import java.util.Comparator;
import java.util.List;

public interface IProjectService {

    Project create(String name, String description);

    List<Project> findAll();

    List<Project> findAll(Comparator<Project> comparator);

    void clear();

    Project findById(String id);

    Project findByName(String name);

    Project findByIndex(Integer index);

    Project removeById(String id);

    Project removeByName(String name);

    Project removeByIndex(Integer index);

    Project updateById(String id, String name, String desc);

    Project updateByIndex(Integer index, String name, String desc);

    Project updateStatusById(String id, Status status);

    Project updateStatusByName(String name, Status status);

    Project updateStatusByIndex(Integer index, Status status);

    boolean isIndex(Integer index);

}
