package ru.smochalkin.tm.comparator;

import ru.smochalkin.tm.api.model.IHasEndDate;
import java.util.Comparator;

public class ComparatorByEndDate implements Comparator<IHasEndDate> {

    public final static ComparatorByEndDate INSTANCE = new ComparatorByEndDate();

    private ComparatorByEndDate() {
    }

    @Override
    public int compare(IHasEndDate o1, IHasEndDate o2) {
        if (o1.getEndDate() == null) return 1;
        if (o2.getEndDate() == null) return -1;
        return o1.getEndDate().compareTo(o2.getEndDate());
    }

}
