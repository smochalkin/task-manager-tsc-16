package ru.smochalkin.tm.repository;

import ru.smochalkin.tm.api.repository.ICommandRepository;
import ru.smochalkin.tm.constant.ArgumentConst;
import ru.smochalkin.tm.constant.TerminalConst;
import ru.smochalkin.tm.model.Command;

public class CommandRepository implements ICommandRepository {

    public static final Command ABOUT = new Command(
            TerminalConst.ABOUT, ArgumentConst.ABOUT,
            "Display developer info."
    );

    public static final Command VERSION = new Command(
            TerminalConst.VERSION, ArgumentConst.VERSION,
            "Display version."
    );

    public static final Command INFO = new Command(
            TerminalConst.INFO, ArgumentConst.INFO,
            "Display system information."
    );

    public static final Command HELP = new Command(
            TerminalConst.HELP, ArgumentConst.HELP,
            "Display list of terminal commands."
    );
    public static final Command ARGUMENTS = new Command(
            TerminalConst.ARGUMENTS, ArgumentConst.ARGUMENTS,
            "Display list of arguments."
    );
    public static final Command COMMANDS = new Command(
            TerminalConst.COMMANDS, ArgumentConst.COMMANDS,
            "Display list of commands."
    );

    public static final Command EXIT = new Command(
            TerminalConst.EXIT, null,
            "Close the program."
    );

    public static final Command TASK_LIST = new Command(
            TerminalConst.TASK_LIST, null,
            "Display list of tasks."
    );

    public static final Command TASK_CREATE = new Command(
            TerminalConst.TASK_CREATE, null,
            "Create new task."
    );

    public static final Command TASK_CLEAR = new Command(
            TerminalConst.TASK_CLEAR, null,
            "Remove all tasks."
    );

    public static final Command PROJECT_LIST = new Command(
            TerminalConst.PROJECT_LIST, null,
            "Display list of projects."
    );

    public static final Command PROJECT_CREATE = new Command(
            TerminalConst.PROJECT_CREATE, null,
            "Create new project."
    );

    public static final Command PROJECT_CLEAR = new Command(
            TerminalConst.PROJECT_CLEAR, null,
            "Remove all projects."
    );

    public static final Command PROJECT_SHOW_BY_NAME = new Command(
            TerminalConst.PROJECT_SHOW_BY_NAME, null,
            "Show project by name."
    );

    public static final Command PROJECT_SHOW_BY_ID = new Command(
            TerminalConst.PROJECT_SHOW_BY_ID, null,
            "Show project by id."
    );

    public static final Command PROJECT_SHOW_BY_INDEX = new Command(
            TerminalConst.PROJECT_SHOW_BY_INDEX, null,
            "Show project by index."
    );

    public static final Command PROJECT_REMOVE_BY_NAME = new Command(
            TerminalConst.PROJECT_REMOVE_BY_NAME, null,
            "Remove project by name."
    );

    public static final Command PROJECT_REMOVE_BY_ID = new Command(
            TerminalConst.PROJECT_REMOVE_BY_ID, null,
            "Remove project by id."
    );

    public static final Command PROJECT_REMOVE_BY_INDEX = new Command(
            TerminalConst.PROJECT_REMOVE_BY_INDEX, null,
            "Remove project by index."
    );

    public static final Command PROJECT_UPDATE_BY_ID = new Command(
            TerminalConst.PROJECT_UPDATE_BY_ID, null,
            "Update project by id."
    );

    public static final Command PROJECT_UPDATE_BY_INDEX = new Command(
            TerminalConst.PROJECT_UPDATE_BY_INDEX, null,
            "Update project by index."
    );

    public static final Command PROJECT_START_BY_NAME = new Command(
            TerminalConst.PROJECT_START_BY_NAME, null,
            "Start project by name."
    );

    public static final Command PROJECT_START_BY_ID = new Command(
            TerminalConst.PROJECT_START_BY_ID, null,
            "Start project by id."
    );

    public static final Command PROJECT_START_BY_INDEX = new Command(
            TerminalConst.PROJECT_START_BY_INDEX, null,
            "Start project by index."
    );

    public static final Command PROJECT_COMPLETE_BY_NAME = new Command(
            TerminalConst.PROJECT_COMPLETE_BY_NAME, null,
            "Complete project by name."
    );

    public static final Command PROJECT_COMPLETE_BY_ID = new Command(
            TerminalConst.PROJECT_COMPLETE_BY_ID, null,
            "Complete project by id."
    );

    public static final Command PROJECT_COMPLETE_BY_INDEX = new Command(
            TerminalConst.PROJECT_COMPLETE_BY_INDEX, null,
            "Complete project by index."
    );

    public static final Command PROJECT_STATUS_UPDATE_BY_ID = new Command(
            TerminalConst.PROJECT_STATUS_UPDATE_BY_ID, null,
            "Update project status by id."
    );

    public static final Command PROJECT_STATUS_UPDATE_BY_NAME = new Command(
            TerminalConst.PROJECT_STATUS_UPDATE_BY_NAME, null,
            "Update project status by name."
    );

    public static final Command PROJECT_STATUS_UPDATE_BY_INDEX = new Command(
            TerminalConst.PROJECT_STATUS_UPDATE_BY_INDEX, null,
            "Update project status by index."
    );

    public static final Command TASK_START_BY_NAME = new Command(
            TerminalConst.TASK_START_BY_NAME, null,
            "Start task by name."
    );

    public static final Command TASK_START_BY_ID = new Command(
            TerminalConst.TASK_START_BY_ID, null,
            "Start task by id."
    );

    public static final Command TASK_START_BY_INDEX = new Command(
            TerminalConst.TASK_START_BY_INDEX, null,
            "Start task by index."
    );

    public static final Command TASK_COMPLETE_BY_NAME = new Command(
            TerminalConst.TASK_COMPLETE_BY_NAME, null,
            "Complete task by name."
    );

    public static final Command TASK_COMPLETE_BY_ID = new Command(
            TerminalConst.TASK_COMPLETE_BY_ID, null,
            "Complete task by id."
    );

    public static final Command TASK_COMPLETE_BY_INDEX = new Command(
            TerminalConst.TASK_COMPLETE_BY_INDEX, null,
            "Complete task by index."
    );

    public static final Command TASK_STATUS_UPDATE_BY_ID = new Command(
            TerminalConst.TASK_STATUS_UPDATE_BY_ID, null,
            "Update task status by id."
    );

    public static final Command TASK_STATUS_UPDATE_BY_NAME = new Command(
            TerminalConst.TASK_STATUS_UPDATE_BY_NAME, null,
            "Update task status by name."
    );

    public static final Command TASK_STATUS_UPDATE_BY_INDEX = new Command(
            TerminalConst.TASK_STATUS_UPDATE_BY_INDEX, null,
            "Update task status by index."
    );

    public static final Command TASK_SHOW_BY_NAME = new Command(
            TerminalConst.TASK_SHOW_BY_NAME, null,
            "Show task by name."
    );

    public static final Command TASK_SHOW_BY_ID = new Command(
            TerminalConst.TASK_SHOW_BY_ID, null,
            "Show task by id."
    );

    public static final Command TASK_SHOW_BY_INDEX = new Command(
            TerminalConst.TASK_SHOW_BY_INDEX, null,
            "Show task by index."
    );

    public static final Command TASK_REMOVE_BY_NAME = new Command(
            TerminalConst.TASK_REMOVE_BY_NAME, null,
            "Remove task by name."
    );

    public static final Command TASK_REMOVE_BY_ID = new Command(
            TerminalConst.TASK_REMOVE_BY_ID, null,
            "Remove task by id."
    );

    public static final Command TASK_REMOVE_BY_INDEX = new Command(
            TerminalConst.TASK_REMOVE_BY_INDEX, null,
            "Remove task by index."
    );

    public static final Command TASK_UPDATE_BY_ID = new Command(
            TerminalConst.TASK_UPDATE_BY_ID, null,
            "Update task by id."
    );

    public static final Command TASK_UPDATE_BY_INDEX = new Command(
            TerminalConst.TASK_UPDATE_BY_INDEX, null,
            "Update task by index."
    );

    public static final Command TASK_BIND_BY_PROJECT_ID = new Command(
            TerminalConst.TASK_BIND_BY_PROJECT_ID, null,
            "Bind task to project by id."
    );

    public static final Command TASK_UNBIND_BY_PROJECT_ID = new Command(
            TerminalConst.TASK_UNBIND_BY_PROJECT_ID, null,
            "Unbind task to project by id."
    );

    public static final Command TASK_SHOW_BY_PROJECT_ID = new Command(
            TerminalConst.TASK_SHOW_BY_PROJECT_ID, null,
            "Show tasks by project id."
    );

    public static final Command[] ALL_COMMANDS = new Command[]{
            ABOUT, VERSION, INFO, HELP, ARGUMENTS, COMMANDS,
            PROJECT_LIST, PROJECT_CREATE, PROJECT_CLEAR,
            PROJECT_SHOW_BY_NAME, PROJECT_SHOW_BY_ID, PROJECT_SHOW_BY_INDEX,
            PROJECT_REMOVE_BY_NAME, PROJECT_REMOVE_BY_ID, PROJECT_REMOVE_BY_INDEX,
            PROJECT_UPDATE_BY_ID, PROJECT_UPDATE_BY_INDEX,
            PROJECT_START_BY_ID, PROJECT_START_BY_NAME, PROJECT_START_BY_INDEX,
            PROJECT_COMPLETE_BY_ID, PROJECT_COMPLETE_BY_NAME, PROJECT_COMPLETE_BY_INDEX,
            PROJECT_STATUS_UPDATE_BY_ID, PROJECT_STATUS_UPDATE_BY_NAME, PROJECT_STATUS_UPDATE_BY_INDEX,
            TASK_LIST, TASK_CREATE, TASK_CLEAR,
            TASK_SHOW_BY_NAME, TASK_SHOW_BY_ID, TASK_SHOW_BY_INDEX,
            TASK_REMOVE_BY_NAME, TASK_REMOVE_BY_ID, TASK_REMOVE_BY_INDEX,
            TASK_UPDATE_BY_ID, TASK_UPDATE_BY_INDEX,
            TASK_START_BY_ID, TASK_START_BY_NAME, TASK_START_BY_INDEX,
            TASK_COMPLETE_BY_ID, TASK_COMPLETE_BY_NAME, TASK_COMPLETE_BY_INDEX,
            TASK_STATUS_UPDATE_BY_ID, TASK_STATUS_UPDATE_BY_NAME, TASK_STATUS_UPDATE_BY_INDEX,
            TASK_BIND_BY_PROJECT_ID, TASK_UNBIND_BY_PROJECT_ID, TASK_SHOW_BY_PROJECT_ID,
            EXIT
    };

    public Command[] getCommands() {
        return ALL_COMMANDS;
    }

}
