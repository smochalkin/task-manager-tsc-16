package ru.smochalkin.tm.repository;

import ru.smochalkin.tm.api.repository.IProjectRepository;
import ru.smochalkin.tm.enumerated.Status;
import ru.smochalkin.tm.exception.entity.ProjectNotFoundException;
import ru.smochalkin.tm.model.Project;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

public class ProjectRepository implements IProjectRepository {

    private final List<Project> projects = new ArrayList<>();

    @Override
    public void add(Project project) {
        projects.add(project);
    }

    @Override
    public void remove(Project project) {
        projects.remove(project);
    }

    @Override
    public List<Project> findAll() {
        return projects;
    }

    @Override
    public List<Project> findAll(Comparator<Project> comparator) {
        List<Project> list = new ArrayList<>(projects);
        list.sort(comparator);
        return list;
    }

    @Override
    public void clear() {
        projects.clear();
    }

    @Override
    public Project findById(String id) {
        for (Project project : projects) {
            if (project.getId().equals(id)) return project;
        }
        throw new ProjectNotFoundException();
    }

    @Override
    public Project findByName(String name) {
        for (Project project : projects) {
            if (project.getName().equals(name)) return project;
        }
        throw new ProjectNotFoundException();
    }

    @Override
    public Project findByIndex(int index) {
        return projects.get(index);
    }

    @Override
    public Project removeById(String id) {
        Project project = findById(id);
        projects.remove(project);
        return project;
    }

    @Override
    public Project removeByName(String name) {
        Project project = findByName(name);
        projects.remove(project);
        return project;
    }

    @Override
    public Project removeByIndex(Integer index) {
        Project project = findByIndex(index);
        projects.remove(project);
        return project;
    }

    @Override
    public Project updateById(String id, String name, String desc) {
        Project project = findById(id);
        project.setName(name);
        project.setDescription(desc);
        return project;
    }

    @Override
    public Project updateByIndex(Integer index, String name, String desc) {
        Project project = findByIndex(index);
        project.setName(name);
        project.setDescription(desc);
        return project;
    }

    @Override
    public int getCount() {
        return projects.size();
    }

    @Override
    public Project updateStatusById(String id, Status status) {
        Project project = findById(id);
        project.setStatus(status);
        updateDate(project, status);
        return project;
    }

    @Override
    public Project updateStatusByName(String name, Status status) {
        Project project = findByName(name);
        project.setStatus(status);
        updateDate(project, status);
        return project;
    }

    @Override
    public Project updateStatusByIndex(int index, Status status) {
        Project project = findByIndex(index);
        project.setStatus(status);
        updateDate(project, status);
        return project;
    }

    private void updateDate(Project project, Status status){
        switch (status){
            case IN_PROGRESS: project.setStartDate(new Date()); break;
            case COMPLETED: project.setEndDate(new Date()); break;
            case NOT_STARTED: project.setStartDate(null); project.setEndDate(null);
        }
    }

}
