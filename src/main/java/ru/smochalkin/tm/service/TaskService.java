package ru.smochalkin.tm.service;

import ru.smochalkin.tm.api.repository.ITaskRepository;
import ru.smochalkin.tm.api.service.ITaskService;
import ru.smochalkin.tm.enumerated.Status;
import ru.smochalkin.tm.exception.empty.EmptyIdException;
import ru.smochalkin.tm.exception.empty.EmptyNameException;
import ru.smochalkin.tm.exception.system.IndexIncorrectException;
import ru.smochalkin.tm.model.Task;

import java.util.Comparator;
import java.util.List;

public class TaskService implements ITaskService {

    private final ITaskRepository taskRepository;

    public TaskService(ITaskRepository taskRepository) {
        this.taskRepository = taskRepository;
    }

    @Override
    public Task create(String name, String description) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        Task task = new Task();
        task.setName(name);
        task.setDescription(description);
        taskRepository.add(task);
        return task;
    }

    @Override
    public List<Task> findAll() {
        return taskRepository.findAll();
    }

    @Override
    public List<Task> findAll(Comparator<Task> comparator) {
        return taskRepository.findAll(comparator);
    }

    @Override
    public void clear() {
        taskRepository.clear();
    }

    @Override
    public Task findById(String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return taskRepository.findById(id);
    }

    @Override
    public Task findByName(String name) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        return taskRepository.findByName(name);
    }

    @Override
    public Task findByIndex(Integer index) {
        if(!isIndex(index)) throw new IndexIncorrectException();
        return taskRepository.findByIndex(index);
    }

    @Override
    public Task removeById(String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return taskRepository.removeById(id);
    }

    @Override
    public Task removeByName(String name) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        return taskRepository.removeByName(name);
    }

    @Override
    public Task removeByIndex(Integer index) {
        if(!isIndex(index)) throw new IndexIncorrectException();
        return taskRepository.removeByIndex(index);
    }

    @Override
    public Task updateById(String id, String name, String desc) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        return taskRepository.updateById(id, name, desc);
    }

    @Override
    public Task updateByIndex(Integer index, String name, String desc) {
        if(!isIndex(index)) throw new IndexIncorrectException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        return taskRepository.updateByIndex(index, name, desc);
    }

    @Override
    public Task updateStatusById(String id, Status status) {
        return taskRepository.updateStatusById(id, status);
    }

    @Override
    public Task updateStatusByName(String name, Status status) {
        return taskRepository.updateStatusByName(name, status);
    }

    @Override
    public Task updateStatusByIndex(Integer index, Status status) {
        return taskRepository.updateStatusByIndex(index, status);
    }

    @Override
    public boolean isIndex(Integer index) {
        if (index == null || index < 0) return false;
        if (index >= taskRepository.getCount()) return false;
        return true;
    }

}
